class Gate {
    String gatenavnet;
    int[] vektene;

    Gate(String navn, int antHus){
        this.gatenavnet = navn;
        this.vektene = new int[antHus];
    }
    Gate(String navn, int[] startvekter){
        this.gatenavnet = navn;
        this.vektene = new int[startvekter.length];
        for(int i = 0; i < startvekter.length; i++) {
            this.vektene[i] = startvekter[i];
        }
    }
    public int[] getVektene(){
        int[] returliste = new int[this.vektene.length];
        for(int i = 0; i < this.vektene.length; i++) {
            returliste[i] = this.vektene[i];
            // System.out.printf("%d",returliste[i]);
        }
        return returliste;
    }

    public void kast(int nr, int vekt){
        this.vektene[nr-1] += vekt;
        return;
    }
    public boolean erFull(int nr){
        // System.out.printf("nr er %d\n", nr);
        if(this.vektene[nr] >= 1000) {
            return true;
        } else return false;
    }
    public void tøm(int nr){
        this.vektene[nr-1] = 0;
        return;
    }
    private int finnSidensMinsteGatenr(boolean like){
        if(like) return 2;
        else return 1;
    }
    public void tøm(boolean like){
        for(int i = this.finnSidensMinsteGatenr(like); i < this.vektene.length; i+=2) {
            this.tøm(i);
        }
    }
    public boolean finsToFulleVedSidenAvHverandre(boolean like){
        boolean forrigefull = false;
        for(int i = this.finnSidensMinsteGatenr(like) -1; i < this.vektene.length; i+=2) {
            // System.out.printf("i er %d", i);
            if(erFull(i) && forrigefull) return true;
            forrigefull = erFull(i);
        }
        return false;
    }
    public String toString(){
        String streng = "Odde side av ";
        streng += this.gatenavnet;
        streng += ":\n";

        for(int i = this.finnSidensMinsteGatenr(false)-1; i< this.vektene.length; i+=2) {
            streng += "    ";
            streng+= i+1;
            streng+= ": ";
            streng+= this.vektene[i];
            streng+= "\n";
        }
        // System.out.printf("%s", streng);
        streng += "Like side av ";
        streng += this.gatenavnet;
        streng += ":\n";

        for(int i = this.finnSidensMinsteGatenr(true)-1; i< this.vektene.length; i+=2) {
            streng += "    ";
            streng+= i+1;
            streng+= ": ";
            streng+= this.vektene[i];
            streng+= "\n";
        }
        return streng;
    }
}
