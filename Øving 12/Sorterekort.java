import java.util.Arrays;

public class Sorterekort {
    public static void main(String[] args){
        Kort[] stokk = new Kort[10];
        stokk[0] = new Kort("hjerter", 2);
        stokk[1] = new Kort("hjerter", 3);
        stokk[2] = new Kort("ruter", 13);
        stokk[3] = new Kort("ruter", 12);
        stokk[4] = new Kort("hjerter", 5);
        stokk[5] = new Kort("hjerter", 4);
        stokk[6] = new Kort("spar", 4);
        stokk[7] = new Kort("kløver", 11);
        stokk[8] = new Kort("kløver", 10);
        stokk[9] = new Kort("kløver", 9);
        Arrays.sort(stokk);

        for(int i = 0; i < stokk.length; i++) {
            System.out.printf("%s\n", stokk[i]);
        }
    }
}
