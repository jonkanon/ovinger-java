class Egenskapssett {
    private final String[] FARGENE = {"spar", "kløver", "hjerter", "ruter"};
    private int minValør;
    private int maxValør;

    Egenskapssett(int minValør, int maxValør){
        this.minValør = minValør;
        this.maxValør = maxValør;
    }
    public int finnAntallValører(){
        return maxValør - minValør + 1;
    }
    public int[] finnValørene(){
        int[] valører = new int[this.finnAntallValører()];
        for(int i = 0; i < this.finnAntallValører(); i++) {
            valører[i] = this.minValør+i;
        }
        return valører;
    }
    public Kort[] finnKortstokken(){
        Kort[] kortstokk = new Kort[this.finnAntallValører()*4];
        String farge;
        for(int i = 0; i < 4; i++) {
            farge = this.FARGENE[i];
            for(int j = 0; j < this.finnAntallValører(); j++) {
                kortstokk[j + i*this.finnAntallValører()] = new Kort(farge, j+this.minValør);
            }
        }
        return kortstokk;
    }
    public int getMinValør(){
        return this.minValør;
    }
    public int getMaxValør(){
        return this.maxValør;
    }
    public int finnAntallKortIKortstokken(){
        return this.finnAntallValører() * this.FARGENE.length;
    }
}
