class Bunke {
    private Egenskapssett egenskapssettet;
    private Kort[] kortene;
    private int antallkort;

    Bunke(Egenskapssett egenskapssett){
        this.egenskapssettet = egenskapssett;
        this.kortene = this.egenskapssettet.finnKortstokken();
        this.antallkort = this.kortene.length;
    }
    public void snu(){
        Kort[] snuddbunke = new Kort[this.antallkort];
        for(int i = 0; i < this.antallkort; i++) {
            snuddbunke[i] = kortene[this.antallkort-1-i];
        }
        this.kortene = snuddbunke;
    }
    public void stokk(){
        //konverterer til liste så vi kan bruke shuffle-metoden
        java.util.List<Kort> liste = java.util.Arrays.asList(this.kortene);
        java.util.Collections.shuffle(liste);

        //konverterer tilbake til tabell
        this.kortene = liste.toArray(new Kort[this.antallkort]);
    }
    public Kort fjernToppen(){
        Kort øversteKort = this.kortene[this.antallkort-1];
        this.kortene[this.antallkort-1] = new Kort("", 0);
        this.antallkort--;
        return øversteKort;
    }
    public int getAntallKort(){
        return this.antallkort;
    }
    public String toString(){
        String returstreng = "";
        for(int i = 0; i < this.antallkort; i++) {
            returstreng += this.kortene[i];
            if(!(i==this.antallkort-1)  ) {
                returstreng += " - ";
            }
        }
        return returstreng;
    }
}
