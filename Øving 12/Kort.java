class Kort implements Comparable<Kort> {
    private String farge;
    private int valør;

    Kort(String farge, int valør){
        this.farge = farge;
        this.valør = valør;
    }

    public String toString(){
        return farge + " " + Integer.toString(valør);
    }

    public boolean equals(Kort annetkort){
        if (annetkort.valør == this.valør) return true;
        return false;
    }

    public int compareTo(Kort annetkort){
        if(this.valør>annetkort.valør) return 1;
        else if(this.valør == annetkort.valør) return 0;
        else return -1;
    }
}
