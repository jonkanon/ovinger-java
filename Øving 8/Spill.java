class Spill {
    Barn spiller1;
    Barn spiller2;
    Kortstokk stokk = new Kortstokk();

    Spill(Barn spiller1, Barn spiller2){
        this.spiller1 = spiller1;
        this.spiller2 = spiller2;
    }
    public void skrivUtStilling(){
        System.out.printf("%s har %d poeng. ", spiller1.hentNavn(), spiller1.hentPoeng());
        System.out.printf("%s har %d poeng.\n", spiller2.hentNavn(), spiller2.hentPoeng());
    }
    public void spillOmgang(){
        spiller1.trekk(stokk);
        spiller2.trekk(stokk);

        spiller1.bytt(spiller2);
        spiller1.sammenlign(spiller2);
    }
}
