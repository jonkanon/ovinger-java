class Barn {
    private final String navn;
    private char bokstav;
    int poeng;

    Barn(String navn){
        this.navn = navn;
        this.poeng = 0;
    }

    public String hentNavn(){
        return this.navn;
    }

    public int hentPoeng(){
        return this.poeng;
    }
    public void leggTilPoeng(){
        this.poeng++;
    }

    public char hentBokstav(){
        return this.bokstav;
    }
    public void settBokstav(char nyBokstav){
        this.bokstav = nyBokstav;
    }

    public void trekk(Kortstokk stokk){
        this.bokstav = stokk.finnKort();
        System.out.printf("%s trakk %c. ", this.navn, this.bokstav);
    }

    public boolean vilBeggeBytte(Barn annetBarn){
        return this.bokstav > 'M' && annetBarn.hentBokstav() > 'M';
    }

    public void bytt(Barn annetBarn){
        if(vilBeggeBytte(annetBarn)) {
            System.out.printf("De byttet.\n");
            char midlertidig = this.bokstav;
            this.bokstav = annetBarn.hentBokstav();
            annetBarn.settBokstav(midlertidig);
            return;
        }
        System.out.printf("De byttet ikke.\n");
    }
    public void sammenlign(Barn annetBarn){
        if(this.bokstav < annetBarn.hentBokstav()) this.leggTilPoeng();
        else if(this.bokstav > annetBarn.hentBokstav()) annetBarn.leggTilPoeng();

    }
}
