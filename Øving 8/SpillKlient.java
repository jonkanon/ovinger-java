import java.util.Scanner;

class SpillKlient {
    public static void main(String[] args){
        Scanner inn = new Scanner(System.in);

        tømSkjerm();
        System.out.printf("Heisann!\n");
        System.out.printf("Spiller 1, tast inn ditt navn: ");
        Barn spiller1 = new Barn(inn.nextLine());
        System.out.printf("Spiller 2, tast inn ditt navn: ");
        Barn spiller2 = new Barn(inn.nextLine());
        Spill spill = new Spill(spiller1, spiller2);
        tømSkjerm();

        boolean vilSlutte = false;
        int valg;
        while(!vilSlutte) {

            System.out.printf("Hva vil du gjøre?\n(1) Spill en runde\n(2) Avslutt\n> ");
            valg = inn.nextInt();
            inn.nextLine();
            tømSkjerm();

            switch(valg) {
            case 1:
                spill.spillOmgang();
                spill.skrivUtStilling();
                break;
            case 2:
                vilSlutte = true;
                break;
            }
        }

        inn.close();
    }
    public static void tømSkjerm(){
        System.out.printf("\033[H\033[2J");
    }

}
