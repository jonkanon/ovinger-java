import java.util.Scanner;

class SkuddAar {
    public static void main(String[] args) throws java.io.IOException {
        Scanner inn = new Scanner(System.in);
        System.out.print("Skriv inn et år: ");
        int år = inn.nextInt();
        boolean skuddår = false;

        if(år % 4 == 0) {
            skuddår=true;
            if(år % 100 == 0) {
                if(år % 400 == 0) skuddår=true;
                else skuddår = false;
            }
        }

        if(skuddår) System.out.println(år + " er et skuddår.");
        else System.out.println(år + " er ikke et skuddår.");

        inn.close();
    }
}
