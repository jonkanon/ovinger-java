import java.util.Scanner;

class EnkelNorsk {
    public static void main(String[] args){
        Scanner inn = new Scanner(System.in);
        System.out.print("Skriv inn en tekst: ");
        String setning = inn.nextLine();

        boolean enkelnorsk = false;

        if(setning.indexOf('æ') >= 0 ||
           setning.indexOf('ø') >= 0 ||
           setning.indexOf('å') >= 0 ) enkelnorsk = true;

        if(setning.indexOf('c') >= 0 ||
           setning.indexOf('q') >= 0 ||
           setning.indexOf('w') >= 0 ||
           setning.indexOf('x') >= 0 ||
           setning.indexOf('z') >= 0 ) enkelnorsk = false;


        if(enkelnorsk) System.out.println("teksten er på enkelnorsk");
        else System.out.println("teksten er ikke på enkelnorsk");

        inn.close();
    }
}
