import java.util.Scanner;

class InneholderC {
    public static void main(String[] args){
        Scanner inn = new Scanner(System.in);
        System.out.print("Skriv inn en tekst: ");
        String setning = inn.nextLine();

        int pos = setning.indexOf('c');

        if(pos >= 0) System.out.println("Den første c-en ligger på posisjon " + pos);
        else System.out.println("Setningen innehodler ingen c-er");

        inn.close();
    }
}
