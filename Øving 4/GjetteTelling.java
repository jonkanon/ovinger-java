import java.util.Scanner;

class GjetteTelling {
    public static void main(String[] args){
        Scanner inn = new Scanner(System.in);
        System.out.print("Skriv inn en tekst: ");
        String setning = inn.nextLine();

        System.out.print("Skriv inn et tegn: ");
        char tegn = inn.nextLine().charAt(0);
        int forekomster = 0;

        //sjekk hvor mange ganger tegn forekommer i String
        for(int i = 0; i < setning.length(); i++) {
            if(setning.charAt(i) == tegn) {
                forekomster++;
            }
        }

        // System.out.printf("forekomster: %d\n", forekomster);

        System.out.print("Vil du gjette? ");
        String vilGjette = inn.nextLine();

        //lar bruker gjette så lenge brukeren har skrevet "ja" til å gjette
        while(vilGjette.equals("ja")) {
            System.out.printf("Gjett hvor mange '%c' det er i \"%s\"! ", tegn, setning);

            //hvis bruker gjetter riktig
            if(inn.nextInt() == forekomster) {
                System.out.printf("Antall '%c' i \"%s\" er %d.\n", tegn, setning, forekomster);
                inn.close();
                return;
            } else {
                // tar med neste linje etter heltallet et par linjer over.
                // denne linjen er tom og skaper kluss lengre nede om vi ikke tar den her.
                inn.nextLine();

                System.out.print("Det var galt. Vil du gjette mer? ");
                vilGjette = inn.nextLine();
            }

        }
        //skriver ut svaret om brukeren ikke vil gjette mer
        System.out.printf("Antall '%c' i \"%s\" er %d.\n", tegn, setning, forekomster);
        inn.close();
        return;
    }
}
