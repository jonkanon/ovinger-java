/*
Dette programmet regner et antall sekunder om til timer, minutter, og sekunder
*/

import static javax.swing.JOptionPane.*;

class TimerMinutterSekunder{
  public static void main(String[] args){
    String lestSekunder = showInputDialog("Antall sekunder: ");


    int totaltSekunder = Integer.parseInt(lestSekunder);

    int timer = totaltSekunder/3600;
    totaltSekunder -= timer*3600;
    int minutter = totaltSekunder/60;
    totaltSekunder -= minutter*60;
    String utskrift = timer +":"+minutter+":"+totaltSekunder;

    showMessageDialog(null, utskrift);
  }
}

/*
eksempel:
Antall sekunder: 6234

1:43:54
*/
