/*
Dette programmet regner ut totalt antall sekunder av timer, minutter, og sekunder
*/

import static javax.swing.JOptionPane.*;

class TotaltSekunder{
  public static void main(String[] args){
    String timer = showInputDialog("antall timer: ");
    String minutter = showInputDialog("antall minutter: ");
    String sekunder = showInputDialog("antall sekunder: ");

    int totaltSekunder = Integer.parseInt(timer)*3600;
    totaltSekunder += Integer.parseInt(minutter)*60;
    totaltSekunder += Integer.parseInt(sekunder);

    String utskrift = "Totalt antall sekunder er: " + totaltSekunder;
    showMessageDialog(null, utskrift);

  }
}

/*
Eksempel:
antall timer: 1
antall minutter: 2
antall sekunder: 30

Totalt antall sekunder er: 3750.0
*/
