/*
Dette programmet sammenligner et heltall, et tegn, og en tekst med noe brukeren skriver inn.
*/

import static javax.swing.JOptionPane.*;

class Gjett {
  public static void main(String[] args){
    int heltall = 23;
    char tegn = 'g';
    String tekst = "skjera bagera";
    showMessageDialog(null, "Et hemmelig heltall, et hemmelig tegn og en hemmelig tekst er blitt valgt. Gjett hva de er!");

    String heltallGjett = showInputDialog("Skriv et heltall: ");
    boolean riktigGjett = Integer.parseInt(heltallGjett)==heltall;
    showMessageDialog(null, "Det hemmelige tallet er " + heltall + ". Du skrev " + heltallGjett + ". Sammenligningsresultat: " + riktigGjett + ".");

    String tegnGjett = showInputDialog("Skriv et tegn: ");
    riktigGjett = tegn == tegnGjett.charAt(0);
    showMessageDialog(null, "Det hemmelige tegnet er " + tegn + ". Du skrev " + tegnGjett + ". Sammenligningsresultat: " + riktigGjett + ".");

    String tekstGjett = showInputDialog("Skriv en tekst: ");
    riktigGjett = tekst.equals(tekstGjett);
    showMessageDialog(null, "Den hemmelige teksten er " + tekst + ". Du skrev " + tekstGjett + ". Sammenligningsresultat: " + riktigGjett + ".");
  }
}
/*
eksempel:
Et hemmelig heltall, et hemmelig tegn og en hemmelig tekst er blitt valgt. Gjett hva de er!
Skriv et heltall: 23
Det hemmelige tallet er 23. Du skrev 23. Sammenligningsresultat: true
Skriv et tegn: c
Det hemmelige tegnet er g. Du skrev c. Sammenligningsresultat: false
Skriv en tekst: heisann
Den hemmelige teksten er skjera bagera. Du skrev heisann. Sammenligningsresultat: false


*/
