/*
Dette programmet regner om grader fahrenheit til grader celsius
*/
import java.util.Scanner;
import static javax.swing.JOptionPane.*;

class Celsiusberegning {
  public static void main(String[] args) throws java.io.IOException {
    Scanner inn = new Scanner(System.in);

    System.out.print("Grader (fahrenheit): ");
    double fahrenheit = inn.nextDouble();
    double desimal = 100;
    double celsius = Math.round(((fahrenheit - 32)*5/9)*desimal)/desimal;
    System.out.println(fahrenheit + " grader fahrenheit er lik " + celsius + " grader celsius.");

    inn.close();
  }
}

/*
Kjøring av programmet:
Grader (fahrenheit): 98
98.0 grader fahrenheit er lik 36.666666666666664 grader celsius.
*/
