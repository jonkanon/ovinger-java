import java.util.Scanner;

class Passordklient {
    public static void main(String[] args){
        Scanner inn = new Scanner(System.in);
        System.out.printf("Heisann! \n");
        System.out.printf("Tast inn et brukernavn: ");
        String brukernavn = inn.nextLine();
        System.out.printf("Tast inn et passord: ");
        String passord = inn.nextLine();

        Bruker bruker = new Bruker(brukernavn, passord);
        // System.out.printf("%s %s", brukernavn, passord);

        boolean kjører = true;
        int valg;
        while(kjører) {
            System.out.printf("Hva vil du gjøre?\n(1) Se Bruker-objektet\n(2) Se Passordhjelper-objektet\n(3) Logge inn\n(4) Logge ut\n(5) Avslutte\n> ");
            valg = inn.nextInt();

            switch (valg) {
            case 1: //Se brukeren
                System.out.printf("%s\n\n", bruker.toString());
                break;
            case 2: //Se hjelperen
                System.out.printf("%s\n\n", bruker.hentHjelper());
                break;
            case 3: //Logge inn
                inn.nextLine();
                System.out.printf("Tast brukernavnet: ");
                brukernavn = inn.nextLine();
                System.out.printf("Tast passordet: ");
                passord = inn.nextLine();
                bruker.loggInn(brukernavn, passord);
                break;
            case 4: //Logge ut
                bruker.loggUt();
                break;
            case 5: //Avslutte
                System.out.printf("Farvel! \n");
                kjører = false;
                break;
            }
        }
        inn.close();
    }
}
