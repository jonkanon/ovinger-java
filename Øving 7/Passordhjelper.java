class Passordhjelper {
    private String passordet;
    Passordtegn pt = new Passordtegn();
    Passordhjelper(String forslaget){
        // this.passordet = pt.fjernulovligetegn(forslaget);
        this.passordet = this.fjernUlovligeTegn(forslaget);
        // System.out.printf("%s %s\n", forslaget, this.passordet);
    }

    private String fjernUlovligeTegn(String tekst){
        String lovligStreng = "";
        for(int i = 0; i < tekst.length(); i++) {
            if(pt.erPassordtegn(tekst.charAt(i))) lovligStreng += tekst.charAt(i);
        }
        return lovligStreng;
    }

    public String toString(){
        String streng = "Passord: ";
        streng += this.passordet;
        streng += ", Passordstyrke: ";
        streng += this.finnPassordstyrken();
        return streng;
    }

    public String hentPassord(){
        return this.passordet;
    }

    public int finnPassordstyrken(){
        int styrke = 0;
        if(this.passordet.length() >= 8) styrke++;
        boolean storBokstav = false, litenBokstav = false, siffer = false, spestegn = false;
        for(int i = 0; i < this.passordet.length(); i++) {
            if(pt.erStorBokstav(this.passordet.charAt(i))) storBokstav = true;
            if(pt.erLitenBokstav(this.passordet.charAt(i))) litenBokstav = true;
            if(pt.erSiffer(this.passordet.charAt(i))) siffer = true;
            if(pt.erSpesialtegn(this.passordet.charAt(i))) spestegn = true;
        }
        if(storBokstav) styrke++;
        if(litenBokstav) styrke++;
        if(siffer) styrke++;
        if(spestegn) styrke++;

        return styrke;
    }

}
