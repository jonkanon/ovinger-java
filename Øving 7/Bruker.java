class Bruker {
    private String brukernavn;
    private Passordhjelper hjelperen;
    private boolean påLogget;

    Bruker(String brukernavn, String passord){
        this.brukernavn = brukernavn;
        this.hjelperen = new Passordhjelper(passord);
        this.påLogget = false;
    }

    public void loggInn(String navn, String passord){
        if( (navn.equals(this.brukernavn)) && (passord.equals(hjelperen.hentPassord()))) this.påLogget = true;
    }
    public void loggUt(){
        this.påLogget = false;
    }
    public String toString(){
        String streng = "";
        streng += "Brukernavn: ";
        streng += this.brukernavn;
        streng += ", Pålogget: ";
        if(this.påLogget) streng += "true";
        if(!this.påLogget) streng += "false";
        return streng;
    }
    public String hentHjelper(){
        return this.hjelperen.toString();
    }
}
