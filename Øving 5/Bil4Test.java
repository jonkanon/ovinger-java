class Bil4Test {
    public static void main(String[] args) {

        /* 1. programsetning: Lager et Bil1-objekt (med referanse) minBil
           vha. standardkonstruktøren: */
        Bil4 minBil = new Bil4("VD-12345", "Volvo", 2002);
        Bil4 dinBil = new Bil4("BF-93566", "Fiat", 1990);

        minBil.start();


        /* 2. programsetning: Skriver ut innholdet i minBils objektvariabler: */
        System.out.println("regnr " + minBil.getRegnr()
                           + ", merke " + minBil.getMerke()
                           + ", årsmodell " + minBil.getÅrsmodell()
                           + ", hastighet " + minBil.getHastighet()
                           + ", motorenIGang " + minBil.isMotorenIGang());
        System.out.println("regnr " + dinBil.getRegnr()
                           + ", merke " + dinBil.getMerke()
                           + ", årsmodell " + dinBil.getÅrsmodell()
                           + ", hastighet " + dinBil.getHastighet()
                           + ", motorenIGang " + dinBil.isMotorenIGang());
        dinBil.start();

        minBil.setHastighet(111);
        dinBil.setHastighet(222);

        minBil.økFarten(29);
        dinBil.brems(11);
        System.out.println("regnr " + minBil.getRegnr()
                           + ", merke " + minBil.getMerke()
                           + ", årsmodell " + minBil.getÅrsmodell()
                           + ", hastighet " + minBil.getHastighet()
                           + ", motorenIGang " + minBil.isMotorenIGang());
        System.out.println("regnr " + dinBil.getRegnr()
                           + ", merke " + dinBil.getMerke()
                           + ", årsmodell " + dinBil.getÅrsmodell()
                           + ", hastighet " + dinBil.getHastighet()
                           + ", motorenIGang " + dinBil.isMotorenIGang());
        minBil.stopp();
        dinBil.stopp();
        System.out.println("regnr " + minBil.getRegnr()
                           + ", merke " + minBil.getMerke()
                           + ", årsmodell " + minBil.getÅrsmodell()
                           + ", hastighet " + minBil.getHastighet()
                           + ", motorenIGang " + minBil.isMotorenIGang());
        System.out.println("regnr " + dinBil.getRegnr()
                           + ", merke " + dinBil.getMerke()
                           + ", årsmodell " + dinBil.getÅrsmodell()
                           + ", hastighet " + dinBil.getHastighet()
                           + ", motorenIGang " + dinBil.isMotorenIGang());
    }
}
