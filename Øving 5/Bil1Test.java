class Bil1Test {
		public static void main(String[] args) {

				/* 1. programsetning: Lager et Bil1-objekt (med referanse) minBil
				   vha. standardkonstruktøren: */
				Bil1 minBil = new Bil1();
				Bil1 dinBil = new Bil1();

				/* 2. programsetning: Skriver ut innholdet i minBils objektvariabler: */
				System.out.println("regnr " + minBil.regnr
								   + ", merke " + minBil.merke
								   + ", årsmodell " + minBil.årsmodell
								   + ", hastighet " + minBil.hastighet
								   + ", motorenIGang " + minBil.motorenIGang);
				System.out.println("regnr " + dinBil.regnr
								   + ", merke " + dinBil.merke
								   + ", årsmodell " + dinBil.årsmodell
								   + ", hastighet " + dinBil.hastighet
								   + ", motorenIGang " + dinBil.motorenIGang);

				minBil.hastighet=111;
				dinBil.hastighet=222;

				System.out.println("regnr " + minBil.regnr
								   + ", merke " + minBil.merke
								   + ", årsmodell " + minBil.årsmodell
								   + ", hastighet " + minBil.hastighet
								   + ", motorenIGang " + minBil.motorenIGang);
				System.out.println("regnr " + dinBil.regnr
								   + ", merke " + dinBil.merke
								   + ", årsmodell " + dinBil.årsmodell
								   + ", hastighet " + dinBil.hastighet
								   + ", motorenIGang " + dinBil.motorenIGang);

		}
}
