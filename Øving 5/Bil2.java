class Bil2 {
		private String regnr;
		private String merke;
		private int årsmodell;
		private int hastighet;
		private boolean motorenIGang;

		public String getRegnr(){
				return regnr;
		}
		public String getMerke(){
				return merke;
		}
		public int getÅrsmodell(){
				return årsmodell;
		}
		public int getHastighet() {
				return hastighet;
		}
		public boolean isMotorenIGang(){
				return motorenIGang;
		}
		public void setHastighet(int hastighet){
				this.hastighet = hastighet;
		}
}
