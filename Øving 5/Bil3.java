class Bil3 {
		private String regnr;
		private String merke;
		private int årsmodell;
		private int hastighet;
		private boolean motorenIGang;

		public String getRegnr(){
				return regnr;
		}
		public String getMerke(){
				return merke;
		}
		public int getÅrsmodell(){
				return årsmodell;
		}
		public int getHastighet() {
				return hastighet;
		}
		public boolean isMotorenIGang(){
				return motorenIGang;
		}
		public void setHastighet(int hastighet){
				this.hastighet = hastighet;
		}

		public Bil3(String regnr, String merke, int årsmodell){
				this.regnr = regnr;
				this.merke = merke;
				this.årsmodell= årsmodell;
				this.hastighet =0;
				this.motorenIGang = false;

		}
}
