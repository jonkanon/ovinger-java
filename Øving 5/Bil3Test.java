class Bil3Test {
		public static void main(String[] args) {

				/* 1. programsetning: Lager et Bil1-objekt (med referanse) minBil
				   vha. standardkonstruktøren: */
				Bil3 minBil = new Bil3("VD-12345", "Volvo", 2002);
				Bil3 dinBil = new Bil3("BF-93566", "Fiat", 1990);

				/* 2. programsetning: Skriver ut innholdet i minBils objektvariabler: */
				System.out.println("regnr " + minBil.getRegnr()
								   + ", merke " + minBil.getMerke()
								   + ", årsmodell " + minBil.getÅrsmodell()
								   + ", hastighet " + minBil.getHastighet()
								   + ", motorenIGang " + minBil.isMotorenIGang());
				System.out.println("regnr " + dinBil.getRegnr()
								   + ", merke " + dinBil.getMerke()
								   + ", årsmodell " + dinBil.getÅrsmodell()
								   + ", hastighet " + dinBil.getHastighet()
								   + ", motorenIGang " + dinBil.isMotorenIGang());

				minBil.setHastighet(111);
				dinBil.setHastighet(222);

				System.out.println("regnr " + minBil.getRegnr()
								   + ", merke " + minBil.getMerke()
								   + ", årsmodell " + minBil.getÅrsmodell()
								   + ", hastighet " + minBil.getHastighet()
								   + ", motorenIGang " + minBil.isMotorenIGang());
				System.out.println("regnr " + dinBil.getRegnr()
								   + ", merke " + dinBil.getMerke()
								   + ", årsmodell " + dinBil.getÅrsmodell()
								   + ", hastighet " + dinBil.getHastighet()
								   + ", motorenIGang " + dinBil.isMotorenIGang());

		}
}
