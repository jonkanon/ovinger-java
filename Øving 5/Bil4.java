class Bil4 {
    private final String regnr;
    private final String merke;
    private final int årsmodell;
    private int hastighet;
    private boolean motorenIGang;

    public String getRegnr(){
        return regnr;
    }
    public String getMerke(){
        return merke;
    }
    public int getÅrsmodell(){
        return årsmodell;
    }
    public int getHastighet() {
        return hastighet;
    }
    public boolean isMotorenIGang(){
        return motorenIGang;
    }
    public void setHastighet(int hastighet){
        this.hastighet = hastighet;
    }
    public void start(){
        this.hastighet = 0;
        this.motorenIGang = true;
    }
    public void økFarten(int økning){
        this.hastighet += økning;
    }
    public void brems(int minskning){
        this.hastighet -= minskning;
    }
    public void stopp(){
        this.hastighet=0;
        this.motorenIGang = false;
    }


    public Bil4(String regnr, String merke, int årsmodell){
        this.regnr = regnr;
        this.merke = merke;
        this.årsmodell= årsmodell;
        this.hastighet =0;
        this.motorenIGang = false;

    }
}
