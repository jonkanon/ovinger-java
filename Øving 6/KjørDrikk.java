import java.util.Scanner;

class KjørDrikk {
    public static void main(String[] args){
        Scanner inn = new Scanner(System.in);
        Drikk vin = new Drikk("vin", 12);
        Drikk øl = new Drikk("øl", 4.7);
        Drikk konjakk = new Drikk("konjakk", 43);

        System.out.printf("Hvor mye veier du/kg?\n> ");
        double vekt = inn.nextDouble();
        inn.nextLine();
        char kjønnSvar = 'a';
        boolean mann = false;
        while (!(kjønnSvar == 'J' || kjønnSvar == 'N')) {
            System.out.printf("\nEr du mann? (J/N)\n> ");
            kjønnSvar = inn.nextLine().charAt(0);
        }
        if (kjønnSvar == 'J') mann = true;

        System.out.printf("\nVelg en drikk eller avslutt\n(1) Øl\n(2) Vin\n(3) Konjakk\n(4) Avslutt\n> ");
        int valg = inn.nextInt();
        int ml = 0;
        Drikk valgtDrikke = øl;
        while (valg != 4) {
            System.out.printf("\nHvor mye vil du drikke/ml?\n> ");
            ml = inn.nextInt();
            switch (valg) {
            case 1:
                valgtDrikke = øl;
                break;
            case 2:
                valgtDrikke = vin;
                break;
            case 3:
                valgtDrikke = konjakk;
                break;
            }

            double kons = valgtDrikke.finnAlkoholkonsentrasjonIBlod(ml, vekt, mann);
            System.out.printf("Din alkoholkonsentrasjon vil bli %f.\n", kons);

            boolean lovÅKjøreBil = true;
            if(kons>0.2) lovÅKjøreBil = false;
            if(lovÅKjøreBil) System.out.printf("Da har du lov å kjøre bil i Norge.\n");
            else System.out.printf("Da har du ikke lov å kjøre bil i Norge.\n");


            System.out.printf("\nVelg en drikk eller avslutt\n(1) Øl\n(2) Vin\n(3) Konjakk\n(4) Avslutt\n> ");
            valg = inn.nextInt();
        }

        inn.close();
    }
}
