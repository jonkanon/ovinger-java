/*
   Aar
   -----------------------
   - int år{readonly}
   -----------------------
 + Aar(int årstall)
 + int getÅr()
 + int beregnIFjor()
 + int beregnNesteÅr()
 + int beregnomNoenÅr(int antall)
 + boolean isSkuddår()
 */

class Aar {
    private final int år;

    public Aar(int årstall){
        this.år = årstall;
    }
    public int getÅr(){
        return this.år;
    }
    public int beregnIFjor(){
        return this.år - 1;
    }
    public int beregnNesteÅr(){
        return this.år + 1;
    }
    public int beregnOmNoenÅr(int antall){
        return this.år + antall;
    }
    public boolean isSkuddår(){
        boolean skuddår = false;
        if(this.år % 4 == 0) {
            skuddår = true;
            if(this.år % 100 == 0) {
                if(this.år % 400 == 0) skuddår=true;
                else skuddår = false;
            }
        }
        return skuddår;
    }
}
