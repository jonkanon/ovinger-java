class Drikk {
    private String navn;
    private double alkoholprosent;

    Drikk(String navn, double prosent){
        this.navn = navn;
        this.alkoholprosent = prosent;
    }

    public String finnNavn(){
        return this.navn;
    }
    public double finnAlkoholProsent(){
        return this.alkoholprosent;
    }
    public double finnGramAlkohol(int ml){
        return alkoholprosent/100*0.79*ml;
    }
    public double finnAlkoholkonsentrasjonIBlod(int ml, double kroppsvekt, boolean mann){
        double w;
        if(mann) w = 0.68;
        else w = 0.55;
        return this.finnGramAlkohol(ml) / kroppsvekt*w;
    }

}
