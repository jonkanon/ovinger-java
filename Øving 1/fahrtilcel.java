/*
Dette programmet regner om grader fahrenheit til grader celsius
*/


class Celsiusberegning {
  public static void main(String[] args){
    double fahrenheit = 98;
    double celsius = (fahrenheit - 32)*5/9;
    System.out.println(fahrenheit + " grader fahrenheit er lik " + celsius + " grader celsius.");
  }
}

/*
Kjøring av programmet:
98.0 grader fahrenheit er lik 36.666666666666664 grader celsius.
*/
